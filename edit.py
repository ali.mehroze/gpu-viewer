import webapp2
import jinja2
from google.appengine.api import users
import os
from google.appengine.ext import ndb
from myuser import MyUser
from gpu_info import Gpu


JINJA_ENVIRONMENT = jinja2.Environment(
loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
extensions=['jinja2.ext.autoescape'],
autoescape=True
)
JINJA_ENVIRONMENT.globals['uri_for'] = webapp2.uri_for

class Edit(webapp2.RequestHandler):
	def get(self,key_id):
		self.response.headers['Content-Type'] = 'text/html'
		search_key= ndb.Key('Gpu',key_id)
		search_key.get()


		template_data={
			"search_key":search_key.get()
		}


		template = JINJA_ENVIRONMENT.get_template('edit.html')
		self.response.write(template.render(template_data))



	def post(self):
		self.response.headers['Content-Type'] = 'text/html'
		#code here
		if self.request.get('button')=='Update':
			name = self.request.get('name')
			manufacturer = self.request.get('manufacturer')
			date_issued = self.request.get('date_issued')
			
			if self.request.get('geometryShader') == 'on':

				geometryShader = True
			else:
				geometryShader = False

			if self.request.get('tesselationShader') == 'on':
				tesselationShader = True
			else:
				tesselationShader =False

			if self.request.get('shaderInt16') == 'on':
				shaderInt16 =  True
			else:
				shaderInt16 =  False
			
			if self.request.get('sparseBinding')== 'on':
				sparseBinding = True
			else:
				sparseBinding = False

			if self.request.get('textureCompressionETC2') == 'on':
				textureCompressionETC2 = True
			else:
				textureCompressionETC2 = False

			if self.request.get('vertexPipelineStoresAndAtomics') == 'on':
				vertexPipelineStoresAndAtomics = True
			else:
				vertexPipelineStoresAndAtomics = False

			# Create key for search
			new_gpu_key=ndb.Key('Gpu',name)
			new_gpu = new_gpu_key.get()
			new_gpu.manufacturer = manufacturer
			new_gpu.geometryShader = geometryShader
			new_gpu.tesselationShader = tesselationShader
			new_gpu.shaderInt16 = shaderInt16
			new_gpu.sparseBinding = sparseBinding
			new_gpu.textureCompressionETC2 = textureCompressionETC2
			new_gpu.vertexPipelineStoresAndAtomics = vertexPipelineStoresAndAtomics

				
			new_gpu.put()

			self.redirect('/')
		
		elif self.request.get('button') == 'Cancel':
			self.redirect('/')
