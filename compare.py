import webapp2
import jinja2
from google.appengine.api import users
import os
from google.appengine.ext import ndb
from myuser import MyUser
from gpu_info import Gpu


JINJA_ENVIRONMENT = jinja2.Environment(
loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
extensions=['jinja2.ext.autoescape'],
autoescape=True
)
JINJA_ENVIRONMENT.globals['uri_for'] = webapp2.uri_for

class Compare(webapp2.RequestHandler):
	def get(self):
		self.redirect('/')

	def post(self):
		comparison_list = self.request.get('compare_check', allow_multiple=True)
		
		gpu_details = []
		for index in comparison_list:
			gpu_info_key = ndb.Key('Gpu', index)
			gpu_info = gpu_info_key.get()
			gpu_details.append(gpu_info)

			template_data3 = {'comparison_list' : gpu_details}

		template = JINJA_ENVIRONMENT.get_template('compare.html')
		self.response.write(template.render(template_data3))

