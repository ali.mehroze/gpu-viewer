from google.appengine.ext import ndb

class Gpu(ndb.Model):

	
	manufacturer = ndb.StringProperty()
	date_issued = ndb.DateTimeProperty(auto_now_add=True)
	geometryShader = ndb.BooleanProperty()
	tesselationShader = ndb.BooleanProperty()
	shaderInt16 = ndb.BooleanProperty()
	sparseBinding = ndb.BooleanProperty()
	textureCompressionETC2 = ndb.BooleanProperty()
	vertexPipelineStoresAndAtomics = ndb.BooleanProperty()

