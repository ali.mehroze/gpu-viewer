import webapp2
import jinja2
from google.appengine.api import users
import os
from google.appengine.ext import ndb
from myuser import MyUser
from gpu_info import Gpu

JINJA_ENVIRONMENT = jinja2.Environment(
loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
extensions=['jinja2.ext.autoescape'],
autoescape=True
)
JINJA_ENVIRONMENT.globals['uri_for'] = webapp2.uri_for


class View(webapp2.RequestHandler):
	def get(self,key_id):
		self.response.headers['Content-Type'] = 'text/html'
		search_key= ndb.Key('Gpu',key_id)
		search_key.get()


		template_data={
			"search_key":search_key.get()
		}


		template = JINJA_ENVIRONMENT.get_template('view.html')
		self.response.write(template.render(template_data))

