import webapp2
import jinja2
from google.appengine.api import users
import os
from google.appengine.ext import ndb
from myuser import MyUser
from gpu_info import Gpu
from edit import Edit
from view import View
from search import Search
from compare import Compare

JINJA_ENVIRONMENT = jinja2.Environment(
loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
extensions=['jinja2.ext.autoescape'],
autoescape=True
)
JINJA_ENVIRONMENT.globals['uri_for'] = webapp2.uri_for

class MainPage(webapp2.RequestHandler):
	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		# URL that will contain a login or logout link
		url = ''
		url_string = ''
		welcome_message = 'Welcome back'
		# pull the current user from the request
		user = users.get_current_user()
		debug = ''

		if user:
			url= users.create_logout_url(self.request.uri)
			url_string = 'logout'

			myuser_key = ndb.Key('MyUser',user.user_id())
			myuser = myuser_key.get()

			if myuser == None:
				welcome_message = 'Welcome to the application'

				myuser = MyUser(id = user.user_id())

				myuser.put()
				debug = Gpu.query().fetch(keys_only = True)
				
		else:
			url = users.create_login_url(self.request.uri)
			url_string = 'Login'
		
		if user:
			debug = Gpu.query().fetch()

		template_values ={
			'url':url,
			'url_string': url_string,
			'user': user,
			'welcome_message':welcome_message,
			'debug': debug
			
		}

		
		# pull the template file and ask jinja to render
		# it with the given template values
		template = JINJA_ENVIRONMENT.get_template('main.html')
		self.response.write(template.render(template_values))


	def post(self):
		self.response.headers['Content-Type'] = 'text/html'
		#code here
		name = self.request.get('name')
		manufacturer = self.request.get('manufacturer')
		
		if self.request.get('geometryShader') == 'on':

			geometryShader = True
		else:
			geometryShader = False

		if self.request.get('tesselationShader') == 'on':
			tesselationShader = True
		else:
			tesselationShader =False

		if self.request.get('shaderInt16') == 'on':
			shaderInt16 =  True
		else:
			shaderInt16 =  False
		
		if self.request.get('sparseBinding')== 'on':
			sparseBinding = True
		else:
			sparseBinding = False

		if self.request.get('textureCompressionETC2') == 'on':
			textureCompressionETC2 = True
		else:
			textureCompressionETC2 = False

		if self.request.get('vertexPipelineStoresAndAtomics') == 'on':
			vertexPipelineStoresAndAtomics = True
		else:
			vertexPipelineStoresAndAtomics = False


		if name:
			#Create key for search
			new_gpu_key=ndb.Key(Gpu,name)
			if new_gpu_key.get() == None:
				new_gpu = Gpu(id = name, manufacturer = manufacturer, 
					geometryShader= geometryShader, tesselationShader = tesselationShader, shaderInt16 = shaderInt16, sparseBinding = sparseBinding, 
					textureCompressionETC2 = textureCompressionETC2, vertexPipelineStoresAndAtomics= vertexPipelineStoresAndAtomics)
				
				new_gpu.put()
		
		self.redirect('/')

app = webapp2.WSGIApplication([
	webapp2.Route(r'/', handler=MainPage, name = 'mainpage'),
	webapp2.Route(r'/compare', handler=Compare, name='compare'), 
	webapp2.Route(r'/search', handler=Search, name='search'), 
	webapp2.Route(r'/view/<key_id:\w+>', handler=View, name='view'), 
	webapp2.Route(r'/edit/<key_id:\w+>', handler=Edit, name='edit'),
	webapp2.Route(r'/edit', handler=Edit)],
	debug=True)
