import webapp2
import jinja2
from google.appengine.api import users
import os
from google.appengine.ext import ndb
from myuser import MyUser
from gpu_info import Gpu
from edit import Edit
from view import View

JINJA_ENVIRONMENT = jinja2.Environment(
loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
extensions=['jinja2.ext.autoescape'],
autoescape=True
)
JINJA_ENVIRONMENT.globals['uri_for'] = webapp2.uri_for


class Search(webapp2.RequestHandler):
	def get(self):
		self.redirect('/')


	def post(self):
		self.response.headers['Content-Type'] = 'text/html'

		gpu_search_list = Gpu.query()
		if self.request.get('geometryShader_2') == 'on':

			geometryShader = True
			gpu_search_list = gpu_search_list.filter(Gpu.geometryShader==geometryShader)
		else:
			geometryShader = False

		if self.request.get('tesselationShader_2') == 'on':
			tesselationShader = True
			gpu_search_list = gpu_search_list.filter(Gpu.tesselationShader==tesselationShader)
		else:
			tesselationShader =False

		if self.request.get('shaderInt16_2') == 'on':
			shaderInt16 =  True
			gpu_search_list = gpu_search_list.filter(Gpu.shaderInt16==shaderInt16)
		else:
			shaderInt16 =  False
		
		if self.request.get('sparseBinding_2')== 'on':
			sparseBinding = True
			gpu_search_list = gpu_search_list.filter(Gpu.sparseBinding==sparseBinding)

		else:
			sparseBinding = False

		if self.request.get('textureCompressionETC2_2') == 'on':
			
			textureCompressionETC2 = True
			gpu_search_list = gpu_search_list.filter(Gpu.textureCompressionETC2==textureCompressionETC2)
		
		else:
			textureCompressionETC2 = False

		if self.request.get('vertexPipelineStoresAndAtomics_2') == 'on':
			vertexPipelineStoresAndAtomics = True
			gpu_search_list = gpu_search_list.filter(Gpu.vertexPipelineStoresAndAtomics==vertexPipelineStoresAndAtomics)
		
		else:
			vertexPipelineStoresAndAtomics = False

		template_search_values = { 'results' : gpu_search_list}

		template = JINJA_ENVIRONMENT.get_template('search.html')
		self.response.write(template.render(template_search_values))
		


